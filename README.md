# leaf-shop
Leaf shop is an e-shop made using **Spring Boot**, **Spring Security**, 
**Spring Data JPA** with **H2 Database** and **Thymeleaf** with **Bootstrap** for the view layer.

[[_TOC_]]

## Screenshots
![index screenshot](images/index.png)
![product screenshot](images/product.png)
![cart screenshot](images/cart.png)
![checkout screenshot](images/checkout.png)
![login screenshot](images/login.png "Login")
![signup screenshot](images/signup.png "Sign Up")

## Run
Clone the repo: 
```
$ https://gitlab.com/leet1cecream/leaf-shop.git
```

### Run using maven

Start the appliation:
```
$ mvn spring-boot:run
```

### Package with maven and run as a jar

Package the application:
```
$ mvn clean package
```

Run it as a jar:
```
$ java -jar target/leaf-shop*.jar
```

## Logins
| E-mail          | Password  | Role  | Description                                                                      |
|-----------------|-----------|-------|----------------------------------------------------------------------------------|
| user@user.com   | user1234  | USER  | Represents the average shopper. Can add items to his shopping cart and checkout. |
| admin@admin.com | admin1234 | ADMIN | Represents the websites admin. Can add, edit and delete products.                |

## License
Project is based on **MIT License**. You can read about the license <a href="LICENSE">here</a>.
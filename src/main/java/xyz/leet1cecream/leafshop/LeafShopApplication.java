package xyz.leet1cecream.leafshop;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LeafShopApplication {

    public static void main(String[] args) {
        SpringApplication.run(LeafShopApplication.class, args);
    }

}

package xyz.leet1cecream.leafshop.bootstrap;

import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import xyz.leet1cecream.leafshop.persistance.model.Category;
import xyz.leet1cecream.leafshop.persistance.model.Product;
import xyz.leet1cecream.leafshop.persistance.model.User;
import xyz.leet1cecream.leafshop.service.CategoryService;
import xyz.leet1cecream.leafshop.service.ProductService;
import xyz.leet1cecream.leafshop.service.UserService;

import java.util.Random;

@Component
public class DataLoader implements CommandLineRunner {

    static final String LOREM_IPSUM = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod " +
            "tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation " +
            "ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in " +
            "voluptate velit esse cillum dolore eu fugiat nulla pariatur.";

    private final Random random = new Random();

    private final UserService userService;

    private final ProductService productService;

    private final CategoryService categoryService;

    public DataLoader(UserService userService, ProductService productService, CategoryService categoryService) {
        this.userService = userService;
        this.productService = productService;
        this.categoryService = categoryService;
    }

    @Override
    public void run(String... args) {
        createUserAccount();
        createAdminAccount();
        createCategories();
        createProducts();
    }

    private void createCategories() {
        for(int i = 0; i < 10; ++i) {
            Category category = new Category();
            category.setName("Category " + i);
            categoryService.save(category);
        }
    }

    private void createProducts() {
        int categoryIndex = 0;
        for(Category category : categoryService.findAll()) {
            for(int i = 0; i < 10; ++i) {
                Product product = new Product();
                product.setName("Product " + (10*categoryIndex+i+1));
                product.setDescription(LOREM_IPSUM);
                product.setPrice(Math.round(random.nextInt(100, 1000)));
                product.setImageUrl("https://www.pngplay.com/wp-content/uploads/2/Green-Leaves-PNG-Photos.png");
                product.setCategory(category);
                productService.save(product);
            }
            categoryIndex++;
        }
    }

    private void createAdminAccount() {
        User user = new User();
        user.setEmail("admin@admin.com");
        user.setPassword("admin1234");
        user.setPasswordConfirmation("admin1234");
        user.setFirstName("admin");
        user.setLastName("admin");

        userService.save(user);
    }

    private void createUserAccount() {
        User user = new User();
        user.setEmail("user@user.com");
        user.setPassword("user1234");
        user.setPasswordConfirmation("user1234");
        user.setFirstName("user");
        user.setLastName("user");

        userService.save(user);
    }

}

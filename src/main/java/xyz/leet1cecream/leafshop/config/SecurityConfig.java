package xyz.leet1cecream.leafshop.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

@Configuration
@EnableWebSecurity
public class SecurityConfig {

    @Bean
    public SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {
        http
                .authorizeHttpRequests(auth -> auth
                        .requestMatchers(AntPathRequestMatcher.antMatcher("/"),
                                AntPathRequestMatcher.antMatcher("/index"),
                                AntPathRequestMatcher.antMatcher("/product/*"),
                                AntPathRequestMatcher.antMatcher("/about"),
                                AntPathRequestMatcher.antMatcher("/tos"),
                                AntPathRequestMatcher.antMatcher("/privacy"),
                                AntPathRequestMatcher.antMatcher("/login"),
                                AntPathRequestMatcher.antMatcher("/signup"),
                                AntPathRequestMatcher.antMatcher("/error"),
                                AntPathRequestMatcher.antMatcher("/css/**"),
                                AntPathRequestMatcher.antMatcher("/js/**"),
                                AntPathRequestMatcher.antMatcher("/images/**"),
                                AntPathRequestMatcher.antMatcher("/favicon.ico")).permitAll()
                        .requestMatchers(AntPathRequestMatcher.antMatcher("/cart/**"),
                                AntPathRequestMatcher.antMatcher("/cart/**/**"),
                                AntPathRequestMatcher.antMatcher("/checkout"),
                                AntPathRequestMatcher.antMatcher("/user"),
                                AntPathRequestMatcher.antMatcher("/user/orders")).authenticated()
                        .requestMatchers(AntPathRequestMatcher.antMatcher("/product/*/**"),
                                AntPathRequestMatcher.antMatcher("/h2-console/**")).hasRole("ADMIN")
                )
                .headers(headers -> headers.frameOptions().disable())
                .csrf(csrf -> csrf.ignoringRequestMatchers(AntPathRequestMatcher.antMatcher("/h2-console/**")).disable())
                .formLogin()
                .loginPage("/login")
                .usernameParameter("email")
                .defaultSuccessUrl("/")
                .failureUrl("/login?error")
                .defaultSuccessUrl("/")
                .and()
                .logout()
                .logoutRequestMatcher(AntPathRequestMatcher.antMatcher("/logout"))
                .deleteCookies("JSESSIONID");
        return http.build();
    }

}

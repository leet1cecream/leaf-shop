package xyz.leet1cecream.leafshop.persistance.model;

import jakarta.persistence.*;
import lombok.*;

@Getter @Setter
@Entity
@Table(name = "cart_items")
public class CartItem extends BaseItem {

    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    @JoinColumn(name = "user_id")
    User user;

}

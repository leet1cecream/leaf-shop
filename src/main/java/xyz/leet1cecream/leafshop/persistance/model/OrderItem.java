package xyz.leet1cecream.leafshop.persistance.model;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter @NoArgsConstructor
@Entity
@Table(name = "order_items")
public class OrderItem extends BaseItem {

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "order_id")
    Order order;

    public OrderItem(CartItem cartItem) {
        product = cartItem.getProduct();
        quantity = cartItem.getQuantity();
    }

}

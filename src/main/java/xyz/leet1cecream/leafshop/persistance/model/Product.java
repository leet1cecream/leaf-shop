package xyz.leet1cecream.leafshop.persistance.model;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Positive;
import lombok.*;
import org.hibernate.validator.constraints.URL;

@Getter @Setter
@Entity
@Table(name = "products")
public class Product extends BaseEntity {

    @NotBlank(message = "Name is required.")
    @Column(name = "name")
    private String name;

    @NotBlank(message = "Description is required.")
    @Column(name = "description", length = 2048)
    private String description;

    @NotBlank(message = "Image URL is required.")
    @URL(regexp = "^(https?).*", message = "Invalid URL.")
    @Column(name = "image_url")
    private String imageUrl;

    @NotNull(message = "Price cannot be empty.")
    @Positive(message = "Price must be positive.")
    @Column(name = "price")
    private Integer price;

    @NotNull(message = "Category cannot be empty.")
    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    @JoinColumn(name = "category_id")
    private Category category;

    public String getShortDescription() {
        return description.substring(0, Math.min(description.length(), 110)) + "...";
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }
}

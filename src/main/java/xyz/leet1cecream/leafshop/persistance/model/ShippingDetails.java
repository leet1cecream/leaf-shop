package xyz.leet1cecream.leafshop.persistance.model;

import jakarta.persistence.*;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import lombok.Getter;
import lombok.Setter;

@Getter @Setter
@Entity
@Table(name = "shipping_details")
public class ShippingDetails extends BaseEntity {

    @OneToOne(fetch = FetchType.LAZY, optional = false)
    Order order;

    @NotBlank(message = "First Name is required.")
    @Column(name = "firstName")
    String firstName;

    @NotBlank(message = "Last Name is required.")
    @Column(name = "lastName")
    String lastName;

    @Email(message = "E-mail must be well formed.")
    @NotBlank(message = "E-mail is required.")
    @Column(name = "email")
    String email;

    @NotBlank(message = "Phone is required.")
    @Column(name = "phone")
    String phone;

    @NotBlank(message = "Country is required.")
    @Column(name = "country")
    String country;

    @NotBlank(message = "City is required.")
    @Column(name = "city")
    String city;

    @NotBlank(message = "Street is required.")
    @Column(name = "street")
    String street;

    @NotBlank(message = "Zip Code is required.")
    @Column(name = "zip")
    String zip;

}

package xyz.leet1cecream.leafshop.persistance.model;

import jakarta.persistence.*;
import jakarta.validation.constraints.*;
import lombok.*;
import xyz.leet1cecream.leafshop.validation.ValidPassword;

import java.util.Comparator;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@Getter @Setter
@Entity
@Table(name = "users")
public class User extends BaseEntity {

    @Email(message = "E-mail must be well formed.")
    @NotBlank(message = "E-mail is required.")
    @Column(name = "email")
    private String email;

    @ValidPassword
    @NotBlank(message = "Password is required.")
    @Column(name = "password")
    private String password;

    @Transient
    private String passwordConfirmation;

    @NotBlank(message = "First name is required.")
    @Column(name = "first_name")
    private String firstName;

    @NotBlank(message = "Last name is required.")
    @Column(name = "last_name")
    private String lastName;

    @OneToMany(mappedBy = "user", fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true)
    Set<CartItem> cartItems = new HashSet<>();

    @OneToMany(mappedBy = "user", fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true)
    Set<Order> orders = new HashSet<>();

    public void addProductToCart(Product product) {
        Optional<CartItem> optionalCartItem = cartItems.stream()
                .filter(cartItem -> cartItem.getProduct().equals(product))
                .findFirst();

        if (optionalCartItem.isPresent()) {
            CartItem cartItem = optionalCartItem.get();
            cartItem.setQuantity(cartItem.getQuantity() + 1);
        } else {
            CartItem cartItem = new CartItem();
            cartItem.setProduct(product);
            cartItem.setQuantity(1);
            cartItem.setUser(this);
            cartItems.add(cartItem);
        }
    }

    public void setProductCartQuantity(Product product, Integer quantity) {
        Optional<CartItem> optionalCartItem = cartItems.stream()
                .filter(cartItem -> cartItem.getProduct().equals(product))
                .findFirst();

        if(optionalCartItem.isPresent()) {
            optionalCartItem.get().setQuantity(quantity);
        }
        else {
            CartItem cartItem = new CartItem();
            cartItem.setProduct(product);
            cartItem.setQuantity(quantity);
            cartItem.setUser(this);
            cartItems.add(cartItem);
        }
    }

    public void removeProductFromCart(Product product) {
        cartItems.remove(cartItems.stream().filter(
                cartItem -> cartItem.getProduct().equals(product)).findFirst().orElse(null));
    }

    public void addOrder(Order order) {
        orders.add(order);
    }

    public Order getLatestOrder() {
        return orders.stream().max((Comparator.comparing(BaseEntity::getDateCreated))).orElse(null);
    }

}

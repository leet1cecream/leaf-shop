package xyz.leet1cecream.leafshop.persistance.repository;

import org.springframework.data.repository.CrudRepository;
import xyz.leet1cecream.leafshop.persistance.model.CartItem;
import xyz.leet1cecream.leafshop.persistance.model.User;

public interface CartItemRepository extends CrudRepository<CartItem, Long> {

    Iterable<CartItem> findByUser(User user);
    Iterable<CartItem> findByUserEmail(String email);

}

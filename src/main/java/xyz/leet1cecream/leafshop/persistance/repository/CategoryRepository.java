package xyz.leet1cecream.leafshop.persistance.repository;

import org.springframework.data.repository.CrudRepository;
import xyz.leet1cecream.leafshop.persistance.model.Category;

public interface CategoryRepository extends CrudRepository<Category, Long> {
}

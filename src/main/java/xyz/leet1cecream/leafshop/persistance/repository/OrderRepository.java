package xyz.leet1cecream.leafshop.persistance.repository;

import org.springframework.data.repository.CrudRepository;
import xyz.leet1cecream.leafshop.persistance.model.Order;

public interface OrderRepository extends CrudRepository<Order, Long> {
}

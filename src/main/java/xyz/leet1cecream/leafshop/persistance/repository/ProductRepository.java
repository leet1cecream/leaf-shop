package xyz.leet1cecream.leafshop.persistance.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import xyz.leet1cecream.leafshop.persistance.model.Category;
import xyz.leet1cecream.leafshop.persistance.model.Product;

public interface ProductRepository extends CrudRepository<Product, Long>, PagingAndSortingRepository<Product, Long> {

    Page<Product> findByNameContaining(String name, Pageable pageable);
    Page<Product> findByNameContainingAndCategory(String name, Category category, Pageable pageable);

}

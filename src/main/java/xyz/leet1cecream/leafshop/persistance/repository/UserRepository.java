package xyz.leet1cecream.leafshop.persistance.repository;

import org.springframework.data.repository.CrudRepository;
import xyz.leet1cecream.leafshop.persistance.model.User;

import java.util.Optional;

public interface UserRepository extends CrudRepository<User, Long> {

    Optional<User> findByEmail(String email);
    boolean existsByEmail(String email);

}

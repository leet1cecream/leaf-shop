package xyz.leet1cecream.leafshop.service;

import xyz.leet1cecream.leafshop.persistance.model.CartItem;

public interface CartItemService {

    CartItem save(CartItem cartItem);
    Iterable<CartItem> findByUserEmail(String email);

}

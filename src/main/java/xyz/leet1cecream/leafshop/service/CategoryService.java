package xyz.leet1cecream.leafshop.service;

import xyz.leet1cecream.leafshop.persistance.model.Category;

import java.util.Optional;

public interface CategoryService {

    Category save(Category category);
    Iterable<Category> findAll();
    Optional<Category> findById(Long id);

}

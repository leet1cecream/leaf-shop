package xyz.leet1cecream.leafshop.service;

import xyz.leet1cecream.leafshop.persistance.model.Order;

public interface OrderService {
    Order save(Order order);
}

package xyz.leet1cecream.leafshop.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import xyz.leet1cecream.leafshop.persistance.model.Category;
import xyz.leet1cecream.leafshop.persistance.model.Product;

import java.util.Optional;

public interface ProductService {

    Product save(Product product);
    Iterable<Product> findAll();
    Optional<Product> findById(Long id);
    Page<Product> findByNameContaining(String name, Pageable pageable);
    Page<Product> findByNameContainingAndCategory(String name, Category category, Pageable pageable);
    void deleteById(Long id);

}

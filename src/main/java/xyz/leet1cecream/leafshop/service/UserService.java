package xyz.leet1cecream.leafshop.service;

import xyz.leet1cecream.leafshop.persistance.model.User;

import java.util.Optional;

public interface UserService {

    User save(User user);
    Optional<User> findByEmail(String email);
    boolean existsByEmail(String email);

}

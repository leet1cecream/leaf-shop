package xyz.leet1cecream.leafshop.service.impl;

import org.springframework.stereotype.Service;
import xyz.leet1cecream.leafshop.persistance.model.CartItem;
import xyz.leet1cecream.leafshop.persistance.repository.CartItemRepository;
import xyz.leet1cecream.leafshop.service.CartItemService;

@Service
public class CartItemServiceImpl implements CartItemService {

    private final CartItemRepository cartItemRepository;

    public CartItemServiceImpl(CartItemRepository cartItemRepository) {
        this.cartItemRepository = cartItemRepository;
    }

    @Override
    public CartItem save(CartItem cartItem) {
        return cartItemRepository.save(cartItem);
    }

    @Override
    public Iterable<CartItem> findByUserEmail(String email) {
        return cartItemRepository.findByUserEmail(email);
    }

}

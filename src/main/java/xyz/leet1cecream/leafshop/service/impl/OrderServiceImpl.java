package xyz.leet1cecream.leafshop.service.impl;

import org.springframework.stereotype.Service;
import xyz.leet1cecream.leafshop.persistance.model.Order;
import xyz.leet1cecream.leafshop.persistance.repository.OrderRepository;
import xyz.leet1cecream.leafshop.service.OrderService;

@Service
public class OrderServiceImpl implements OrderService {

    private final OrderRepository orderRepository;

    public OrderServiceImpl(OrderRepository orderRepository) {
        this.orderRepository = orderRepository;
    }

    @Override
    public Order save(Order order) {
        return orderRepository.save(order);
    }
}

package xyz.leet1cecream.leafshop.service.impl;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import xyz.leet1cecream.leafshop.persistance.model.Category;
import xyz.leet1cecream.leafshop.persistance.model.Product;
import xyz.leet1cecream.leafshop.persistance.repository.ProductRepository;
import xyz.leet1cecream.leafshop.service.ProductService;

import java.util.Optional;

@Service
public class ProductServiceImpl implements ProductService {

    private final ProductRepository productRepository;

    public ProductServiceImpl(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @Override
    public Product save(Product product) {
        return productRepository.save(product);
    }

    @Override
    public Iterable<Product> findAll() {
        return productRepository.findAll();
    }

    @Override
    public Optional<Product> findById(Long id) {
        return productRepository.findById(id);
    }

    @Override
    public Page<Product> findByNameContaining(String name, Pageable pageable) {
        return productRepository.findByNameContaining(name, pageable);
    }

    @Override
    public Page<Product> findByNameContainingAndCategory(String name, Category category, Pageable pageable) {
        return productRepository.findByNameContainingAndCategory(name, category, pageable);
    }

    @Override
    public void deleteById(Long id) {
        productRepository.deleteById(id);
    }
}

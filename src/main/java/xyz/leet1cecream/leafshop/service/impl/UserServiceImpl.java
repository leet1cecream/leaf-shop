package xyz.leet1cecream.leafshop.service.impl;

import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import xyz.leet1cecream.leafshop.persistance.model.User;
import xyz.leet1cecream.leafshop.service.UserService;

import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {

    private final xyz.leet1cecream.leafshop.persistance.repository.UserRepository userRepository;
    public final PasswordEncoder passwordEncoder;

    public UserServiceImpl(xyz.leet1cecream.leafshop.persistance.repository.UserRepository userRepository, PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public User save(User user) {
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        user.setPasswordConfirmation(user.getPassword());
        return userRepository.save(user);
    }

    @Override
    public Optional<User> findByEmail(String email) {
        return userRepository.findByEmail(email);
    }

    @Override
    public boolean existsByEmail(String email) {
        return userRepository.existsByEmail(email);
    }

}

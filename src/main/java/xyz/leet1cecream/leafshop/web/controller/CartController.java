package xyz.leet1cecream.leafshop.web.controller;

import jakarta.servlet.http.HttpServletRequest;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import xyz.leet1cecream.leafshop.persistance.model.CartItem;
import xyz.leet1cecream.leafshop.persistance.model.Product;
import xyz.leet1cecream.leafshop.persistance.model.User;
import xyz.leet1cecream.leafshop.service.CartItemService;
import xyz.leet1cecream.leafshop.service.ProductService;
import xyz.leet1cecream.leafshop.service.UserService;

import java.util.*;
import java.util.stream.StreamSupport;

@Controller
@RequestMapping("/cart")
public class CartController {

    private final ProductService productService;
    private final UserService userService;
    private final CartItemService cartItemService;

    public CartController(ProductService productService, UserService userService, CartItemService cartItemService) {
        this.productService = productService;
        this.userService = userService;
        this.cartItemService = cartItemService;
    }

    @GetMapping
    public String cart(Model model, Authentication authentication) {
        Iterable<CartItem> cartItems = cartItemService.findByUserEmail(authentication.getName());
        model.addAttribute("cartItems", cartItems);
        int total = StreamSupport.stream(cartItems.spliterator(), false)
                .mapToInt(cartItem -> cartItem.getQuantity() * cartItem.getProduct().getPrice()).sum();
        model.addAttribute("total", total);
        return "cart";
    }

    @PostMapping
    public String processCart(HttpServletRequest request, Authentication authentication) {
        List<String> requestParameterNames = Collections.list(request.getParameterNames());
        requestParameterNames.forEach(productId -> {
            Integer quantity = Integer.valueOf(request.getParameter(productId));
            Optional<Product> optionalProduct = productService.findById(Long.valueOf(productId));
            Optional<User> optionalUser = userService.findByEmail(authentication.getName());
            if (optionalProduct.isPresent() && optionalUser.isPresent()) {
                User user = optionalUser.get();
                Product product = optionalProduct.get();
                user.setProductCartQuantity(product, quantity);
                userService.save(user);
            }
        });
        return "redirect:/cart";
    }

    @GetMapping("/add")
    public String addProduct(@RequestParam("productId") Optional<Long> productId, Authentication authentication) {
        if (productId.isPresent()) {
            Optional<Product> optionalProduct = productService.findById(productId.get());
            if (optionalProduct.isPresent()) {
                Product product = optionalProduct.get();
                Optional<User> optionalUser = userService.findByEmail(authentication.getName());
                if (optionalUser.isPresent()) {
                    User user = optionalUser.get();
                    user.addProductToCart(product);
                    userService.save(user);
                }
            }
        }
        return "redirect:/cart";
    }

    @GetMapping("/remove")
    public String removeProduct(@RequestParam("productId") Optional<Long> productId, Authentication authentication) {
        if (productId.isPresent()) {
            Optional<Product> optionalProduct = productService.findById(productId.get());
            if (optionalProduct.isPresent()) {
                Product product = optionalProduct.get();
                Optional<User> optionalUser = userService.findByEmail(authentication.getName());
                if (optionalUser.isPresent()) {
                    User user = optionalUser.get();
                    user.removeProductFromCart(product);
                    userService.save(user);
                }
            }
        }
        return "redirect:/cart";
    }

}

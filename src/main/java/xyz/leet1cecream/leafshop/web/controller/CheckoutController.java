package xyz.leet1cecream.leafshop.web.controller;

import jakarta.validation.Valid;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import xyz.leet1cecream.leafshop.persistance.model.Order;
import xyz.leet1cecream.leafshop.persistance.model.OrderItem;
import xyz.leet1cecream.leafshop.persistance.model.ShippingDetails;
import xyz.leet1cecream.leafshop.persistance.model.User;
import xyz.leet1cecream.leafshop.service.UserService;

import java.util.Optional;

@Controller
@RequestMapping("/checkout")
public class CheckoutController {

    private final UserService userService;

    public CheckoutController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping
    public String checkout(Model model, Authentication authentication) {
        Optional<User> optionalUser = userService.findByEmail(authentication.getName());
        if (optionalUser.isEmpty()) {
            return "redirect:/";
        }

        User user = optionalUser.get();

        if (user.getCartItems().isEmpty()) {
            return "redirect:/cart";
        }

        model.addAttribute("shippingDetails", new ShippingDetails());
        model.addAttribute("cartItems", user.getCartItems());
        int total = user.getCartItems().stream()
                .mapToInt(cartItem -> cartItem.getQuantity() * cartItem.getProduct().getPrice()).sum();
        model.addAttribute("total", total);

        return "checkout";
    }

    @PostMapping
    public String processCheckout(Model model, @Valid ShippingDetails shippingDetails, BindingResult bindingResult,
                                  Authentication authentication) {
        Optional<User> optionalUser = userService.findByEmail(authentication.getName());
        if (optionalUser.isEmpty()) {
            return "redirect:/cart";
        }
        User user = optionalUser.get();

        if (bindingResult.hasErrors()) {
            model.addAttribute("cartItems", user.getCartItems());
            int total = user.getCartItems().stream()
                    .mapToInt(cartItem -> cartItem.getQuantity() * cartItem.getProduct().getPrice()).sum();
            model.addAttribute("total", total);
            return "checkout";
        }

        Order order = new Order();
        order.setUser(user);
        shippingDetails.setOrder(order);
        order.setShippingDetails(shippingDetails);
        user.getCartItems().forEach(cartItem -> order.addOrderItem(new OrderItem(cartItem)));
        user.addOrder(order);

        User savedUser = userService.save(user);
        return "redirect:/user/orders/" + savedUser.getLatestOrder().getId();
    }

}

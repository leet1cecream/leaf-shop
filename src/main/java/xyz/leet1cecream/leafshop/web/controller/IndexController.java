package xyz.leet1cecream.leafshop.web.controller;

import jakarta.validation.Valid;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import xyz.leet1cecream.leafshop.persistance.model.Category;
import xyz.leet1cecream.leafshop.persistance.model.Product;
import xyz.leet1cecream.leafshop.persistance.model.User;
import xyz.leet1cecream.leafshop.service.CategoryService;
import xyz.leet1cecream.leafshop.service.ProductService;
import xyz.leet1cecream.leafshop.service.UserService;

import java.util.Optional;

@Controller
public class IndexController {

    private final ProductService productService;
    private final CategoryService categoryService;
    private final UserService userService;

    public IndexController(ProductService productService, CategoryService categoryService, UserService userService) {
        this.productService = productService;
        this.categoryService = categoryService;
        this.userService = userService;
    }

    @GetMapping(value = {"", "/", "/index", "/home"})
    public String index(Model model, @RequestParam("category") Optional<Long> categoryId,
                        @RequestParam("search") Optional<String> search, Pageable pageable) {
        model.addAttribute("categories", categoryService.findAll());

        Page<Product> products = null;
        if (categoryId.isPresent()) {
            Optional<Category> category = categoryService.findById(categoryId.get());
            if (category.isPresent()) {
                model.addAttribute("category", category.get());
                products = productService.findByNameContainingAndCategory(search.orElse(""), category.get(), pageable);
            }
        }
        else {
            products = productService.findByNameContaining(search.orElse(""), pageable);
        }

        model.addAttribute("products", products);
        model.addAttribute("currentPage", products.getNumber());
        model.addAttribute("totalPages", products.getTotalPages());

        return "index";
    }

    @GetMapping("/signup")
    public String signup(Model model) {
        model.addAttribute("user", new User());
        return "signup";
    }

    @PostMapping("/signup")
    public String processSignup(@Valid User user, BindingResult bindingResult) {
        if (user.getPasswordConfirmation().isBlank()) {
            bindingResult.rejectValue("passwordConfirmation", "passwordConfirmationEmpty",
                    "Password confirmation is required.");
        }

        if (!user.getPassword().equals(user.getPasswordConfirmation())) {
            bindingResult.rejectValue("passwordConfirmation", "passwordConfirmationNotMatching",
                    "Passwords must match.");
        }

        if (bindingResult.hasErrors()) {
            return "signup";
        }

        if (userService.existsByEmail(user.getEmail())) {
            bindingResult.rejectValue("email", "alreadyUsed", "E-mail already in use.");
            return "signup";
        }

        userService.save(user);
        return "redirect:/login?reg";
    }

    @GetMapping("/about")
    public String about() {
        return "about";
    }

    @GetMapping("/tos")
    public String tos() {
        return "tos";
    }

    @GetMapping("/privacy")
    public String privacy() {
        return "privacy";
    }

}

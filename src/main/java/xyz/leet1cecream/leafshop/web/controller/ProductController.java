package xyz.leet1cecream.leafshop.web.controller;

import jakarta.validation.Valid;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.server.ResponseStatusException;
import xyz.leet1cecream.leafshop.persistance.model.Product;
import xyz.leet1cecream.leafshop.service.CategoryService;
import xyz.leet1cecream.leafshop.service.ProductService;

import java.util.Optional;

@Controller
@RequestMapping("/product")
public class ProductController {

    private final ProductService productService;
    private final CategoryService categoryService;

    public ProductController(ProductService productService, CategoryService categoryService) {
        this.productService = productService;
        this.categoryService = categoryService;
    }

    @GetMapping("/{productId}")
    public String product(Model model, @PathVariable("productId") Long productId) {

        Optional<Product> product = productService.findById(productId);

        model.addAttribute("product", product.orElseThrow(
                () -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Product not found")));

        return "product";
    }

    @GetMapping("/{productId}/edit")
    public String editProduct(Model model, @PathVariable("productId") Long productId) {

        Optional<Product> product = productService.findById(productId);

        model.addAttribute("product", product.orElseThrow(
                () -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Product not found")));
        model.addAttribute("categories", categoryService.findAll());

        return "editProduct";
    }

    @PostMapping("/{productId}/edit")
    public String processEditProduct(Model model, @PathVariable("productId") Long productId, @Valid Product product,
                                     BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            product.setId(productId);
            model.addAttribute("categories", categoryService.findAll());
            return "editProduct";
        }

        product.setId(productId);
        productService.save(product);

        return "redirect:/product/" + product.id;
    }

    @GetMapping("/{productId}/delete")
    public String delete(Model model, @PathVariable("productId") Long productId) {
        Optional<Product> product = productService.findById(productId);
        model.addAttribute("product", product.orElseThrow(
                () -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Product not found")));
        return "deleteProduct";
    }

    @PostMapping("/{productId}/delete")
    public String processDelete(@PathVariable("productId") Long productId) {
        productService.deleteById(productId);
        return "redirect:/";
    }

    @GetMapping("/new")
    public String newProduct(Model model) {
        model.addAttribute("product", new Product());
        model.addAttribute("categories", categoryService.findAll());
        return "editProduct";
    }

    @PostMapping("/new")
    public String processNewProduct(Model model, @Valid Product product, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("categories", categoryService.findAll());
            return "editProduct";
        }
        Product savedProduct = productService.save(product);
        return "redirect:/product/" + savedProduct.getId();
    }

}

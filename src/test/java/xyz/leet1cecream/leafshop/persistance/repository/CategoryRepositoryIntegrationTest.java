package xyz.leet1cecream.leafshop.persistance.repository;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;

import xyz.leet1cecream.leafshop.persistance.model.Category;

@DataJpaTest
public class CategoryRepositoryIntegrationTest {

    @Autowired
    private CategoryRepository categoryRepository;

    @Autowired
    private TestEntityManager entityManager;

    @Test
    public void whenSaveCategory_thenCategoryIsPersisted() {
        Category category = new Category();
        category.setName("Test Category");
        Category savedCategory = categoryRepository.save(category);
        assertThat(savedCategory.getId()).isNotNull();
    }

    @Test
    public void givenCategoryCreated_whenUpdateCategory_thenCategoryIsUpdated() {
        Category category = new Category();
        category.setName("Test Category");
        Category persistedCategory = entityManager.persist(category);
        entityManager.flush();

        persistedCategory.setName("Updated Category");
        Category updatedCategory = categoryRepository.save(persistedCategory);

        assertThat(updatedCategory.getName()).isEqualTo("Updated Category");
    }

    @Test
    public void givenCategoryCreated_whenFindById_thenReturnCategory() {
        Category category = new Category();
        category.setName("Test Category");
        Category persistedCategory = entityManager.persist(category);
        entityManager.flush();

        Optional<Category> foundCategory = categoryRepository.findById(persistedCategory.getId());

        assertThat(foundCategory).isPresent();
        assertThat(foundCategory.get().getName()).isEqualTo("Test Category");
    }

    @Test
    public void givenCategoryCreated_whenDeleteCategory_thenCategoryIsDeleted() {
        Category category = new Category();
        category.setName("Test Category");
        Category persistedCategory = entityManager.persist(category);
        entityManager.flush();

        categoryRepository.delete(persistedCategory);

        Optional<Category> foundCategory = categoryRepository.findById(persistedCategory.getId());
        assertThat(foundCategory).isEmpty();
    }
}

package xyz.leet1cecream.leafshop.persistance.repository;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import xyz.leet1cecream.leafshop.persistance.model.Category;
import xyz.leet1cecream.leafshop.persistance.model.Product;

@DataJpaTest
public class ProductRepositoryIntegrationTest {

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private CategoryRepository categoryRepository;

    @Autowired
    TestEntityManager entityManager;

    @Test
    public void whenSaveProduct_thenProductIsPersisted() {
        Product product = new Product();
        product.setName("Test Product");
        product.setDescription("Test Description");
        product.setImageUrl("https://testimage.com/test.jpg");
        product.setPrice(100);
        product.setCategory(categoryRepository.save(createTestCategory()));
        productRepository.save(product);

        assertThat(product.getId()).isNotNull();
    }

    @Test
    public void givenProductCreated_whenUpdateProduct_thenProductIsUpdated() {
        Product product = new Product();
        product.setName("Test Product");
        product.setDescription("Test Description");
        product.setImageUrl("https://testimage.com/test.jpg");
        product.setPrice(100);
        product.setCategory(categoryRepository.save(createTestCategory()));
        entityManager.persist(product);
        entityManager.flush();

        product.setName("Updated Test Product");
        product.setDescription("Updated Test Description");
        product.setImageUrl("https://testimage.com/updated.jpg");
        product.setPrice(200);
        productRepository.save(product);

        Product updatedProduct = entityManager.find(Product.class, product.getId());
        assertThat(updatedProduct.getName()).isEqualTo("Updated Test Product");
    }

    @Test
    public void givenProductCreated_whenFindProductById_thenReturnProduct() {
        Product product = new Product();
        product.setName("Test Product");
        product.setDescription("Test Description");
        product.setImageUrl("https://testimage.com/test.jpg");
        product.setPrice(100);
        product.setCategory(categoryRepository.save(createTestCategory()));
        entityManager.persist(product);
        entityManager.flush();

        Optional<Product> foundProduct = productRepository.findById(product.getId());

        assertThat(foundProduct).isPresent();
        assertThat(foundProduct.get()).isEqualTo(product);
    }

    @Test
    public void givenProductCreated_whenFindByNameContaining_thenReturnProducts() {
        Product product = new Product();
        product.setName("Test Product");
        product.setDescription("Test Description");
        product.setImageUrl("https://testimage.com/test.jpg");
        product.setPrice(100);
        product.setCategory(categoryRepository.save(createTestCategory()));
        entityManager.persist(product);
        entityManager.flush();

        Page<Product> foundProducts = productRepository.findByNameContaining("Product", Pageable.unpaged());

        assertThat(foundProducts).isNotEmpty();
        assertThat(foundProducts).contains(product);
    }

    @Test
    public void givenProductCreated_whenDeleteProduct_thenProductIsDeleted() {
        Product product = new Product();
        product.setName("Test Product");
        product.setDescription("Test description");
        product.setImageUrl("https://example.com/image.jpg");
        product.setPrice(1000);
        product.setCategory(categoryRepository.save(createTestCategory()));
        entityManager.persist(product);
        entityManager.flush();

        productRepository.deleteById(product.getId());

        Product deletedProductOptional = entityManager.find(Product.class, product.getId());
        assertThat(deletedProductOptional).isNull();
    }

    Category createTestCategory() {
        Category category = new Category();
        category.setName("Test Category");
        return category;
    }

}

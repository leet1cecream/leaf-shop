package xyz.leet1cecream.leafshop.persistance.repository;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import xyz.leet1cecream.leafshop.persistance.model.User;

@DataJpaTest
public class UserRepositoryIntegrationTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private UserRepository userRepository;

    @Test
    public void whenSaveUser_thenUserIsPersisted() {
        User user = new User();
        user.setEmail("testuser@example.com");
        user.setPassword("testpassword");
        user.setPasswordConfirmation("testpassword");
        user.setFirstName("testname");
        user.setLastName("testname");
        userRepository.save(user);
        assertThat(user.getId()).isNotNull();
    }

    @Test
    public void givenUserPersisted_whenUpdateUser_thenUserIsUpdated() {
        User user = new User();
        user.setEmail("testuser@example.com");
        user.setPassword("testpassword");
        user.setPasswordConfirmation("testpassword");
        user.setFirstName("testname");
        user.setLastName("testname");
        entityManager.persist(user);
        entityManager.flush();

        user.setEmail("newemail@example.com");
        userRepository.save(user);
        entityManager.flush();

        User updatedUser = entityManager.find(User.class, user.getId());
        assertThat(updatedUser.getEmail()).isEqualTo("newemail@example.com");
    }

    @Test
    public void givenUserPersisted_whenFindById_thenReturnUser() {
        User user = new User();
        user.setEmail("testuser@example.com");
        user.setPassword("testpassword");
        user.setPasswordConfirmation("testpassword");
        user.setFirstName("testname");
        user.setLastName("testname");
        entityManager.persist(user);
        entityManager.flush();

        Optional<User> foundUser = userRepository.findById(user.getId());

        assertThat(foundUser).isPresent();
        assertThat(foundUser.get()).isEqualTo(user);
    }

    @Test
    public void givenUserCreated_whenFindByEmail_thenReturnUser() {
        User user = new User();
        user.setEmail("testuser@example.com");
        user.setPassword("testpassword");
        user.setPasswordConfirmation("testpassword");
        user.setFirstName("testname");
        user.setLastName("testname");
        entityManager.persist(user);
        entityManager.flush();

        Optional<User> foundUser = userRepository.findByEmail(user.getEmail());

        assertThat(foundUser).isPresent();
        assertThat(foundUser.get()).isEqualTo(user);
    }

    @Test
    public void givenUserCreated_whenExistsByEmail_thenExists() {
        User user = new User();
        user.setEmail("testuser@example.com");
        user.setPassword("testpassword");
        user.setPasswordConfirmation("testpassword");
        user.setFirstName("testname");
        user.setLastName("testname");
        entityManager.persist(user);
        entityManager.flush();

        assertThat(userRepository.existsByEmail(user.getEmail())).isTrue();
    }

    @Test
    public void givenUserPersisted_whenDeleteUser_thenUserIsDeleted() {
        User user = new User();
        user.setEmail("testuser@example.com");
        user.setPassword("testpassword");
        user.setPasswordConfirmation("testpassword");
        user.setFirstName("testname");
        user.setLastName("testname");
        entityManager.persist(user);
        entityManager.flush();

        userRepository.deleteById(user.getId());
        entityManager.flush();

        User deletedUser = entityManager.find(User.class, user.getId());
        assertThat(deletedUser).isNull();
    }
}
